# Autor : Guilherme Ito
# Ultima alteracao: 11/11/2018
# Todos os testes foram realizados em ambiente MAC OS, utilizando Mozilla Firefox como browser

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import json
import time

# Leitura da entrada json
with open("input.json", "r") as input:
    json_data = json.load(input)

# brew install geckodriver
# Para correcao do erro
# selenium.common.exceptions.WebDriverException: Message: 'geckodriver' executable needs to be in PATH.
# Inicializacao do Firefox
browser = webdriver.Firefox()

try:
    # Criacao de um dicionario para o resultado ser codificado em JSON
    dicionario_resultados = {}
    for item in json_data["google-me"]:

        # Acesso ao google.com
        browser.get('http://www.google.com')

        # Encontar o elemento onde name = "q" (caixa de pesquisa do google)
        inputElement = browser.find_element_by_name("q")

        # Para cada elemento, envia a chave de pesquisa para o google
        inputElement.send_keys(item)

        # Submissao da pesquisa do item chave
        inputElement.submit()

        # Aguarda o titulo do browser mudar e conter o item pesquisado + " - Pesquisa Google" (alterar caso mude o idioma)
        WebDriverWait(browser, 10).until(EC.title_contains(item + " - Pesquisa Google"))
        # Wait para o carregamento dos resutados
        time.sleep(0.5)

        # Encontra os elementos cujo nome da classe eh LC20lb (indica um resultado padrao do google)
        resultados = browser.find_elements_by_class_name("LC20lb")
        lista_resultados = []

        # Para os tres primeiros resultados, colocamos o seu titulo na lista de resultados
        for resultado in resultados[:3]:
            lista_resultados.append(resultado.text)

        # Adicionamos no dicionario a lista de resultados do item chave
        dicionario_resultados[item] = lista_resultados

except:
    print "TEST EXECUTION ERROR"

finally:
    browser.quit()

# Impressao do dicionario de resultados encontrados
print dicionario_resultados

# Insere o dicionario encontrado codificado em json em output.json
with open("output.json", "w") as output:
    json.dump(dicionario_resultados, output, indent = 4)
